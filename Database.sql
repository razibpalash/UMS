USE [master]
GO
/****** Object:  Database [UMSDB]    Script Date: 10/14/2015 2:00:46 AM ******/
CREATE DATABASE [UMSDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UMSDB', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UMSDB.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UMSDB_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UMSDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [UMSDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UMSDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UMSDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UMSDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UMSDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UMSDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UMSDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [UMSDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UMSDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [UMSDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UMSDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UMSDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UMSDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UMSDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UMSDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UMSDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UMSDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UMSDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UMSDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UMSDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UMSDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UMSDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UMSDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UMSDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UMSDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UMSDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UMSDB] SET  MULTI_USER 
GO
ALTER DATABASE [UMSDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UMSDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UMSDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UMSDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [UMSDB]
GO
/****** Object:  Table [dbo].[AssignCourses]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignCourses](
	[AssignCourseId] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[TeacherId] [int] NOT NULL,
	[CreditTaken] [float] NOT NULL,
	[RemaingCredit] [float] NOT NULL,
	[CourseId] [int] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.AssignCourses] PRIMARY KEY CLUSTERED 
(
	[AssignCourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Courses]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Courses](
	[CourseId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Credit] [float] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[SemesterId] [int] NOT NULL,
	[AssignTo] [nvarchar](max) NULL,
	[Semester_SemesterId] [int] NULL,
	[CourseSemister_SemesterId] [int] NULL,
	[Semester_SemesterId1] [int] NULL,
	[Status] [bit] NOT NULL,
	[SemName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Courses] PRIMARY KEY CLUSTERED 
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Days]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Days](
	[DayId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Days] PRIMARY KEY CLUSTERED 
(
	[DayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Departments]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departments](
	[DepartmentId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](7) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Departments] PRIMARY KEY CLUSTERED 
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Designations]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Designations](
	[DesignationId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Designations] PRIMARY KEY CLUSTERED 
(
	[DesignationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EnrollCourses]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnrollCourses](
	[EnrollCourseId] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[EnrollDate] [datetime] NOT NULL,
	[GradeName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.EnrollCourses] PRIMARY KEY CLUSTERED 
(
	[EnrollCourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Enrollments]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enrollments](
	[EnrollmentId] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[StudentId] [int] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[Result] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Enrollments] PRIMARY KEY CLUSTERED 
(
	[EnrollmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Grades]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grades](
	[GradeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Grades] PRIMARY KEY CLUSTERED 
(
	[GradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ResultEntries]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResultEntries](
	[ResultEntryId] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[GradeId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ResultEntries] PRIMARY KEY CLUSTERED 
(
	[ResultEntryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoomAllocations]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoomAllocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[DayId] [int] NOT NULL,
	[From] [nvarchar](max) NULL,
	[To] [nvarchar](max) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.RoomAllocations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rooms](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Rooms] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Semesters]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Semesters](
	[SemesterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Semesters] PRIMARY KEY CLUSTERED 
(
	[SemesterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Students]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[StudentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[ContactNo] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[RegistrationId] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Students] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TeacherCourses]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherCourses](
	[Teacher_TeacherId] [int] NOT NULL,
	[Course_CourseId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.TeacherCourses] PRIMARY KEY CLUSTERED 
(
	[Teacher_TeacherId] ASC,
	[Course_CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Teachers]    Script Date: 10/14/2015 2:00:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teachers](
	[TeacherId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NULL,
	[ContactNo] [nvarchar](max) NOT NULL,
	[DesignationId] [int] NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[CreditTaken] [float] NOT NULL,
 CONSTRAINT [PK_dbo.Teachers] PRIMARY KEY CLUSTERED 
(
	[TeacherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AssignCourses] ON 

INSERT [dbo].[AssignCourses] ([AssignCourseId], [DepartmentId], [TeacherId], [CreditTaken], [RemaingCredit], [CourseId], [Status]) VALUES (1, 1, 1, 20, 17, 1, 0)
INSERT [dbo].[AssignCourses] ([AssignCourseId], [DepartmentId], [TeacherId], [CreditTaken], [RemaingCredit], [CourseId], [Status]) VALUES (2, 2, 2, 12, 9, 4, 0)
SET IDENTITY_INSERT [dbo].[AssignCourses] OFF
SET IDENTITY_INSERT [dbo].[Courses] ON 

INSERT [dbo].[Courses] ([CourseId], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId], [AssignTo], [Semester_SemesterId], [CourseSemister_SemesterId], [Semester_SemesterId1], [Status], [SemName]) VALUES (1, N'cse-101', N'introduction to computer', 3, N'introduction to computer', 1, 1, N'Abdul Kader', NULL, NULL, NULL, 1, N'1st Semester')
INSERT [dbo].[Courses] ([CourseId], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId], [AssignTo], [Semester_SemesterId], [CourseSemister_SemesterId], [Semester_SemesterId1], [Status], [SemName]) VALUES (2, N'cse-102', N'introduction to computer lab', 1.5, N'Information Technology lab', 1, 1, NULL, NULL, NULL, NULL, 1, N'1st Semester')
INSERT [dbo].[Courses] ([CourseId], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId], [AssignTo], [Semester_SemesterId], [CourseSemister_SemesterId], [Semester_SemesterId1], [Status], [SemName]) VALUES (3, N'cse-103', N'Information Technology', 1.5, N'ViewBag.DepartmentId ', 1, 1, NULL, NULL, NULL, NULL, 1, N'1st Semester')
INSERT [dbo].[Courses] ([CourseId], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId], [AssignTo], [Semester_SemesterId], [CourseSemister_SemesterId], [Semester_SemesterId1], [Status], [SemName]) VALUES (4, N'cse-104', N'computer Graphics', 3, N'Computer Graphics', 2, 4, N'Minijul Islam', NULL, NULL, NULL, 1, N'4th Semester')
INSERT [dbo].[Courses] ([CourseId], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId], [AssignTo], [Semester_SemesterId], [CourseSemister_SemesterId], [Semester_SemesterId1], [Status], [SemName]) VALUES (5, N'cse-105', N'machine learning', 3, N'machine learning', 1, 5, NULL, NULL, NULL, NULL, 1, N'5th Semester')
INSERT [dbo].[Courses] ([CourseId], [Code], [Name], [Credit], [Description], [DepartmentId], [SemesterId], [AssignTo], [Semester_SemesterId], [CourseSemister_SemesterId], [Semester_SemesterId1], [Status], [SemName]) VALUES (6, N'cse-106', N'machine learning lab', 1, N'machine learning lab', 2, 4, NULL, NULL, NULL, NULL, 1, N'4th Semester')
SET IDENTITY_INSERT [dbo].[Courses] OFF
SET IDENTITY_INSERT [dbo].[Days] ON 

INSERT [dbo].[Days] ([DayId], [Name]) VALUES (1, N'Saturday')
INSERT [dbo].[Days] ([DayId], [Name]) VALUES (2, N'Sunday')
INSERT [dbo].[Days] ([DayId], [Name]) VALUES (3, N'Monday')
INSERT [dbo].[Days] ([DayId], [Name]) VALUES (4, N'Tuesday')
INSERT [dbo].[Days] ([DayId], [Name]) VALUES (5, N'Wednesday')
INSERT [dbo].[Days] ([DayId], [Name]) VALUES (6, N'Thursday')
INSERT [dbo].[Days] ([DayId], [Name]) VALUES (7, N'Friday')
SET IDENTITY_INSERT [dbo].[Days] OFF
SET IDENTITY_INSERT [dbo].[Departments] ON 

INSERT [dbo].[Departments] ([DepartmentId], [Code], [Name]) VALUES (1, N'CSE ', N'Computer Science and Engineering')
INSERT [dbo].[Departments] ([DepartmentId], [Code], [Name]) VALUES (2, N'IT', N'	Information Technology')
INSERT [dbo].[Departments] ([DepartmentId], [Code], [Name]) VALUES (3, N'ECE', N'Electrical and computer engineering')
SET IDENTITY_INSERT [dbo].[Departments] OFF
SET IDENTITY_INSERT [dbo].[Designations] ON 

INSERT [dbo].[Designations] ([DesignationId], [Name]) VALUES (1, N'Professor')
INSERT [dbo].[Designations] ([DesignationId], [Name]) VALUES (2, N'Assiestent Professor')
INSERT [dbo].[Designations] ([DesignationId], [Name]) VALUES (3, N'Associate Professor')
INSERT [dbo].[Designations] ([DesignationId], [Name]) VALUES (4, N'Lecturer')
SET IDENTITY_INSERT [dbo].[Designations] OFF
SET IDENTITY_INSERT [dbo].[EnrollCourses] ON 

INSERT [dbo].[EnrollCourses] ([EnrollCourseId], [StudentId], [CourseId], [EnrollDate], [GradeName]) VALUES (1, 1, 1, CAST(0x0000A52D00000000 AS DateTime), N'A+')
SET IDENTITY_INSERT [dbo].[EnrollCourses] OFF
SET IDENTITY_INSERT [dbo].[Grades] ON 

INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (1, N'A+')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (2, N'A')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (3, N'A-')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (4, N'B+')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (5, N'B')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (6, N'B-')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (7, N'C+')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (8, N'C')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (9, N'C-')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (10, N'D+')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (11, N'D')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (12, N'D-')
INSERT [dbo].[Grades] ([GradeId], [Name]) VALUES (13, N'F')
SET IDENTITY_INSERT [dbo].[Grades] OFF
SET IDENTITY_INSERT [dbo].[ResultEntries] ON 

INSERT [dbo].[ResultEntries] ([ResultEntryId], [StudentId], [CourseId], [GradeId]) VALUES (1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[ResultEntries] OFF
SET IDENTITY_INSERT [dbo].[RoomAllocations] ON 

INSERT [dbo].[RoomAllocations] ([Id], [DepartmentId], [CourseId], [RoomId], [DayId], [From], [To], [Status]) VALUES (1, 1, 1, 1, 1, N'09:30', N'11:00', 0)
INSERT [dbo].[RoomAllocations] ([Id], [DepartmentId], [CourseId], [RoomId], [DayId], [From], [To], [Status]) VALUES (2, 2, 4, 2, 1, N'09:30', N'11:00', 0)
INSERT [dbo].[RoomAllocations] ([Id], [DepartmentId], [CourseId], [RoomId], [DayId], [From], [To], [Status]) VALUES (3, 1, 2, 12, 2, N'11:15', N'12:45', 0)
INSERT [dbo].[RoomAllocations] ([Id], [DepartmentId], [CourseId], [RoomId], [DayId], [From], [To], [Status]) VALUES (4, 2, 6, 12, 3, N'13:30', N'16:30', 0)
INSERT [dbo].[RoomAllocations] ([Id], [DepartmentId], [CourseId], [RoomId], [DayId], [From], [To], [Status]) VALUES (5, 1, 5, 1, 1, N'13:30', N'15:00', 0)
SET IDENTITY_INSERT [dbo].[RoomAllocations] OFF
SET IDENTITY_INSERT [dbo].[Rooms] ON 

INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (1, N'101')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (2, N'102')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (3, N'103')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (4, N'104')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (5, N'105')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (6, N'201')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (7, N'202')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (8, N'203')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (9, N'204')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (10, N'205')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (11, N'301')
INSERT [dbo].[Rooms] ([RoomId], [Name]) VALUES (12, N'302')
SET IDENTITY_INSERT [dbo].[Rooms] OFF
SET IDENTITY_INSERT [dbo].[Semesters] ON 

INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (1, N'1st Semester')
INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (2, N'2nd Semester')
INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (3, N'3rd Semester')
INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (4, N'4th Semester')
INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (5, N'5th Semester')
INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (6, N'6th Semester')
INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (7, N'7th Semester')
INSERT [dbo].[Semesters] ([SemesterId], [Name]) VALUES (8, N'8th Semester')
SET IDENTITY_INSERT [dbo].[Semesters] OFF
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([StudentId], [Name], [Email], [ContactNo], [Date], [Address], [DepartmentId], [RegistrationId]) VALUES (1, N'palash Chandra Bormon', N'palashcseuits@gmail.com', N'+8801681645431', CAST(0x0000A52D00000000 AS DateTime), N'Baridhara j block,Dhaka, Bangladesh', 1, N'CSE -2015-001')
INSERT [dbo].[Students] ([StudentId], [Name], [Email], [ContactNo], [Date], [Address], [DepartmentId], [RegistrationId]) VALUES (2, N'Tonmoy Shekh', N'tonmoy@gmail.com', N'+8801681645431', CAST(0x0000A52C00000000 AS DateTime), N'Mirpur,Dhaka,Bangladesh', 2, N'IT-2015-001')
INSERT [dbo].[Students] ([StudentId], [Name], [Email], [ContactNo], [Date], [Address], [DepartmentId], [RegistrationId]) VALUES (3, N'Md. Lavlu', N'mdlavlu@gmail.com', N'+8801742645443', CAST(0x0000A52F00000000 AS DateTime), N'Mymenisngh,Bangladesh', 1, N'CSE -2015-002')
INSERT [dbo].[Students] ([StudentId], [Name], [Email], [ContactNo], [Date], [Address], [DepartmentId], [RegistrationId]) VALUES (4, N'Monirul islam rajuk', N'rajuk@gmail.com', N'+8801671032456', CAST(0x0000A52F00000000 AS DateTime), N'Dhaka,Bangladesh', 3, N'ECE-2015-001')
INSERT [dbo].[Students] ([StudentId], [Name], [Email], [ContactNo], [Date], [Address], [DepartmentId], [RegistrationId]) VALUES (5, N'Biplob Chandra Barman', N'biplob@gmail.com', N'+8801671032456', CAST(0x0000A52900000000 AS DateTime), N'mymensingh, Bangaldesh', 3, N'ECE-2015-002')
INSERT [dbo].[Students] ([StudentId], [Name], [Email], [ContactNo], [Date], [Address], [DepartmentId], [RegistrationId]) VALUES (6, N'Ashikur Rahaman', N'ashik@gmail.com', N'+8801742645443', CAST(0x0000A52200000000 AS DateTime), N'rajshahi , Bangaldesh', 2, N'IT-2015-002')
SET IDENTITY_INSERT [dbo].[Students] OFF
SET IDENTITY_INSERT [dbo].[Teachers] ON 

INSERT [dbo].[Teachers] ([TeacherId], [Name], [Address], [Email], [ContactNo], [DesignationId], [DepartmentId], [CreditTaken]) VALUES (1, N'Abdul Kader', N'Dhaka, Bangladesh', N'abdulkader@gmail.com', N'+8801681645431', 2, 1, 20)
INSERT [dbo].[Teachers] ([TeacherId], [Name], [Address], [Email], [ContactNo], [DesignationId], [DepartmentId], [CreditTaken]) VALUES (2, N'Minijul Islam', N'Dhaka, Bangladesh', N'minijul@gmail.com', N'+8801681645431', 4, 2, 12)
SET IDENTITY_INSERT [dbo].[Teachers] OFF
/****** Object:  Index [IX_CourseId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_CourseId] ON [dbo].[AssignCourses]
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepartmentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[AssignCourses]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TeacherId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_TeacherId] ON [dbo].[AssignCourses]
(
	[TeacherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CourseSemister_SemesterId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_CourseSemister_SemesterId] ON [dbo].[Courses]
(
	[CourseSemister_SemesterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepartmentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[Courses]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Semester_SemesterId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_Semester_SemesterId] ON [dbo].[Courses]
(
	[Semester_SemesterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Semester_SemesterId1]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_Semester_SemesterId1] ON [dbo].[Courses]
(
	[Semester_SemesterId1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CourseId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_CourseId] ON [dbo].[EnrollCourses]
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_StudentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_StudentId] ON [dbo].[EnrollCourses]
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepartmentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[Enrollments]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_StudentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_StudentId] ON [dbo].[Enrollments]
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CourseId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_CourseId] ON [dbo].[ResultEntries]
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_GradeId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_GradeId] ON [dbo].[ResultEntries]
(
	[GradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_StudentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_StudentId] ON [dbo].[ResultEntries]
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CourseId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_CourseId] ON [dbo].[RoomAllocations]
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DayId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DayId] ON [dbo].[RoomAllocations]
(
	[DayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepartmentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[RoomAllocations]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_RoomId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_RoomId] ON [dbo].[RoomAllocations]
(
	[RoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepartmentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[Students]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Course_CourseId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_Course_CourseId] ON [dbo].[TeacherCourses]
(
	[Course_CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teacher_TeacherId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_Teacher_TeacherId] ON [dbo].[TeacherCourses]
(
	[Teacher_TeacherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepartmentId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[Teachers]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DesignationId]    Script Date: 10/14/2015 2:00:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_DesignationId] ON [dbo].[Teachers]
(
	[DesignationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssignCourses] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Courses] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[RoomAllocations] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[AssignCourses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssignCourses_dbo.Courses_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([CourseId])
GO
ALTER TABLE [dbo].[AssignCourses] CHECK CONSTRAINT [FK_dbo.AssignCourses_dbo.Courses_CourseId]
GO
ALTER TABLE [dbo].[AssignCourses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssignCourses_dbo.Departments_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO
ALTER TABLE [dbo].[AssignCourses] CHECK CONSTRAINT [FK_dbo.AssignCourses_dbo.Departments_DepartmentId]
GO
ALTER TABLE [dbo].[AssignCourses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssignCourses_dbo.Teachers_TeacherId] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[Teachers] ([TeacherId])
GO
ALTER TABLE [dbo].[AssignCourses] CHECK CONSTRAINT [FK_dbo.AssignCourses_dbo.Teachers_TeacherId]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Courses_dbo.Departments_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_dbo.Courses_dbo.Departments_DepartmentId]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Courses_dbo.Semesters_CourseSemister_SemesterId] FOREIGN KEY([CourseSemister_SemesterId])
REFERENCES [dbo].[Semesters] ([SemesterId])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_dbo.Courses_dbo.Semesters_CourseSemister_SemesterId]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Courses_dbo.Semesters_Semester_SemesterId] FOREIGN KEY([Semester_SemesterId])
REFERENCES [dbo].[Semesters] ([SemesterId])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_dbo.Courses_dbo.Semesters_Semester_SemesterId]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Courses_dbo.Semesters_Semester_SemesterId1] FOREIGN KEY([Semester_SemesterId1])
REFERENCES [dbo].[Semesters] ([SemesterId])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_dbo.Courses_dbo.Semesters_Semester_SemesterId1]
GO
ALTER TABLE [dbo].[EnrollCourses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EnrollCourses_dbo.Courses_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([CourseId])
GO
ALTER TABLE [dbo].[EnrollCourses] CHECK CONSTRAINT [FK_dbo.EnrollCourses_dbo.Courses_CourseId]
GO
ALTER TABLE [dbo].[EnrollCourses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EnrollCourses_dbo.Students_StudentId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Students] ([StudentId])
GO
ALTER TABLE [dbo].[EnrollCourses] CHECK CONSTRAINT [FK_dbo.EnrollCourses_dbo.Students_StudentId]
GO
ALTER TABLE [dbo].[Enrollments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Enrollments_dbo.Departments_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO
ALTER TABLE [dbo].[Enrollments] CHECK CONSTRAINT [FK_dbo.Enrollments_dbo.Departments_DepartmentId]
GO
ALTER TABLE [dbo].[Enrollments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Enrollments_dbo.Students_StudentId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Students] ([StudentId])
GO
ALTER TABLE [dbo].[Enrollments] CHECK CONSTRAINT [FK_dbo.Enrollments_dbo.Students_StudentId]
GO
ALTER TABLE [dbo].[ResultEntries]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ResultEntries_dbo.Courses_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([CourseId])
GO
ALTER TABLE [dbo].[ResultEntries] CHECK CONSTRAINT [FK_dbo.ResultEntries_dbo.Courses_CourseId]
GO
ALTER TABLE [dbo].[ResultEntries]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ResultEntries_dbo.Grades_GradeId] FOREIGN KEY([GradeId])
REFERENCES [dbo].[Grades] ([GradeId])
GO
ALTER TABLE [dbo].[ResultEntries] CHECK CONSTRAINT [FK_dbo.ResultEntries_dbo.Grades_GradeId]
GO
ALTER TABLE [dbo].[ResultEntries]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ResultEntries_dbo.Students_StudentId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Students] ([StudentId])
GO
ALTER TABLE [dbo].[ResultEntries] CHECK CONSTRAINT [FK_dbo.ResultEntries_dbo.Students_StudentId]
GO
ALTER TABLE [dbo].[RoomAllocations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoomAllocations_dbo.Courses_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([CourseId])
GO
ALTER TABLE [dbo].[RoomAllocations] CHECK CONSTRAINT [FK_dbo.RoomAllocations_dbo.Courses_CourseId]
GO
ALTER TABLE [dbo].[RoomAllocations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoomAllocations_dbo.Days_DayId] FOREIGN KEY([DayId])
REFERENCES [dbo].[Days] ([DayId])
GO
ALTER TABLE [dbo].[RoomAllocations] CHECK CONSTRAINT [FK_dbo.RoomAllocations_dbo.Days_DayId]
GO
ALTER TABLE [dbo].[RoomAllocations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoomAllocations_dbo.Departments_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO
ALTER TABLE [dbo].[RoomAllocations] CHECK CONSTRAINT [FK_dbo.RoomAllocations_dbo.Departments_DepartmentId]
GO
ALTER TABLE [dbo].[RoomAllocations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoomAllocations_dbo.Rooms_RoomId] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
GO
ALTER TABLE [dbo].[RoomAllocations] CHECK CONSTRAINT [FK_dbo.RoomAllocations_dbo.Rooms_RoomId]
GO
ALTER TABLE [dbo].[Students]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Students_dbo.Departments_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO
ALTER TABLE [dbo].[Students] CHECK CONSTRAINT [FK_dbo.Students_dbo.Departments_DepartmentId]
GO
ALTER TABLE [dbo].[TeacherCourses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TeacherCourses_dbo.Courses_Course_CourseId] FOREIGN KEY([Course_CourseId])
REFERENCES [dbo].[Courses] ([CourseId])
GO
ALTER TABLE [dbo].[TeacherCourses] CHECK CONSTRAINT [FK_dbo.TeacherCourses_dbo.Courses_Course_CourseId]
GO
ALTER TABLE [dbo].[TeacherCourses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TeacherCourses_dbo.Teachers_Teacher_TeacherId] FOREIGN KEY([Teacher_TeacherId])
REFERENCES [dbo].[Teachers] ([TeacherId])
GO
ALTER TABLE [dbo].[TeacherCourses] CHECK CONSTRAINT [FK_dbo.TeacherCourses_dbo.Teachers_Teacher_TeacherId]
GO
ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Teachers_dbo.Departments_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
GO
ALTER TABLE [dbo].[Teachers] CHECK CONSTRAINT [FK_dbo.Teachers_dbo.Departments_DepartmentId]
GO
ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Teachers_dbo.Designations_DesignationId] FOREIGN KEY([DesignationId])
REFERENCES [dbo].[Designations] ([DesignationId])
GO
ALTER TABLE [dbo].[Teachers] CHECK CONSTRAINT [FK_dbo.Teachers_dbo.Designations_DesignationId]
GO
USE [master]
GO
ALTER DATABASE [UMSDB] SET  READ_WRITE 
GO
