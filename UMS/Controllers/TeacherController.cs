﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS.Models;

namespace UMS.Controllers
{
    public class TeacherController : Controller
    {
        ProjectDBContext db = new ProjectDBContext();
        //
        // GET: /Teacher/
        public ActionResult Index()
        {
            var teachers = db.Teachers.Include(t => t.Department).Include(t => t.Designation);
            return View(teachers.ToList());
        }

        //
        // GET: /Teacher/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code");
            ViewBag.DesignationId = new SelectList(db.Designations, "DesignationId", "Name");
            return View();
        }

        //
        // POST: /Teacher/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TeacherId,Name,Address,Email,ContactNo,DesignationId,DepartmentId,CreditTaken")] Teacher teacher)
        {
            if (ModelState.IsValid)
            {
                if (!teacher.IsExist(teacher.Email)) 
                {
                    db.Teachers.Add(teacher);
                    db.SaveChanges();
                    TempData["Success"] = "Added Successfully!!";
                    return RedirectToAction("Create");
                }
                else
                {
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", teacher.DepartmentId);
                    ViewBag.DesignationId = new SelectList(db.Designations, "DesignationId", "Name", teacher.DesignationId);
                    ViewBag.Message = "Email already Exist.";
                    return View(); 
                }
                
            }

            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", teacher.DepartmentId);
            ViewBag.DesignationId = new SelectList(db.Designations, "DesignationId", "Name", teacher.DesignationId);
            return View(teacher);
        }
    }
}
