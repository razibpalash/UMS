﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS.Models;

namespace UMS.Controllers
{
    public class ResultEntryController : Controller
    {
        ProjectDBContext db = new ProjectDBContext();
        //
        // GET: /ResultEntry/
        public ActionResult Index()
        {
            var resultEntries = db.ResultEntries.Include(x => x.Student).Include(c => c.Course).Include(g => g.Grade);
            return View(resultEntries.ToList());
        }

        //
        // GET: /ResultEntry/Create
        public ActionResult Create()
        {
            ViewBag.StudentId = new SelectList(db.Students, "StudentId" , "RegistrationId");
            ViewBag.CourseId = new SelectList("", "CourseId","Code");
            ViewBag.GradeId = new SelectList(db.Grades, "GradeId","Name");

            return View();
        }

        //
        // POST: /ResultEntry/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ResultEntry resultEntry)
        {
            if (ModelState.IsValid)
            {
                var result =
                db.ResultEntries.Count(s => s.StudentId == resultEntry.StudentId && s.CourseId == resultEntry.CourseId) ==
                0;
                if (result)
                {
                    Grade grade = db.Grades.Where(g => g.GradeId == resultEntry.GradeId).FirstOrDefault();
                    EnrollCourse enrollCourse =
                        db.EnrollCourses.FirstOrDefault(
                            c => c.CourseId == resultEntry.CourseId && c.StudentId == resultEntry.StudentId);

                    if (enrollCourse != null)
                    {
                        enrollCourse.GradeName = grade.Name;
                    }
                    db.ResultEntries.Add(resultEntry);
                    db.SaveChanges();
                    TempData["success"] = "Update Result This Course!!";
                    return RedirectToAction("Create");
                }
                else
                {
                    TempData["Already"] = " Already Updated Result!!";
                    return RedirectToAction("Create");
                }
            }
            ViewBag.StudentId = new SelectList(db.Students, "StudentId", "RegistrationId", resultEntry.StudentId);
            ViewBag.CourseId = new SelectList(db.Courses, "CourseId", "Code", resultEntry.CourseId);
            ViewBag.GradeId = new SelectList(db.Grades, "GradeId", "Name", resultEntry.GradeId);
            return View(resultEntry);
        }

        public PartialViewResult StudentInfoLoad(int? studentId)
        {
            if (studentId != null)
            {
                Student aStudent = db.Students.FirstOrDefault(s => s.StudentId == studentId);
                ViewBag.Name = aStudent.Name;
                ViewBag.Email = aStudent.Email;
                ViewBag.Dept = aStudent.Department.Name;
                return PartialView("~/Views/Shared/_StudentInformation.cshtml");
            }
            else
            {
                return PartialView("~/Views/Shared/_StudentInformation.cshtml");
            }

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public PartialViewResult CourseLoad(int? studentId)
        {
            List<EnrollCourse> enrollmentList = new List<EnrollCourse>();
            List<Course> courseList = new List<Course>();
            if (studentId != null)
            {
                enrollmentList = db.EnrollCourses.Where(e => e.StudentId == studentId).ToList();
                foreach (EnrollCourse anEnrollment in enrollmentList)
                {
                    Course aCourse = db.Courses.FirstOrDefault(c => c.CourseId == anEnrollment.CourseId);
                    courseList.Add(aCourse);
                }
                ViewBag.CourseId = new SelectList(courseList, "CourseId", "Code");
            }
            return PartialView("~/Views/shared/_FilteredCourse.cshtml");
        }

        public ActionResult ViewResult()
        {
            ViewBag.StudentId = new SelectList(db.Students, "StudentId", "RegistrationId");
            return View();
        }

        public PartialViewResult ResultInfoLoad(int? studentId)
        {

            List<EnrollCourse> enrollCourseList = new List<EnrollCourse>();

            if (studentId != null)
            {
                enrollCourseList = db.EnrollCourses.Where(r => r.StudentId == studentId).ToList();

                if (enrollCourseList.Count == 0)
                {
                    Student aStudent = db.Students.Find(studentId);
                    ViewBag.NotEnrolled = " This Student has not Enroll any course.";
                }
            }

            return PartialView("~/Views/shared/_resultInformation.cshtml", enrollCourseList);
        }
    }
}
