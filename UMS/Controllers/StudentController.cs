﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UMS.Models;

namespace UMS.Controllers
{
    public class StudentController : Controller
    {
        ProjectDBContext db = new ProjectDBContext();
        //
        // GET: /Student/
        public ActionResult Index()
        {
            var students = db.Students.Include(s => s.Department);
            return View(students.ToList());
        }

        //
        // GET: /Student/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        //
        // GET: /Student/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code");
            return View();
        }

        //
        // POST: /Student/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StudentId,Name,Email,ContactNo,Date,Address,DepartmentId,RegistrationId")] Student student)
        {
            if (ModelState.IsValid)
            {
                if (!student.IsExist(student.Email))
                {
                    student.RegistrationId = GenerateRegistrationId(student);

                    db.Students.Add(student);
                    db.SaveChanges();
                    TempData["reg"] = student.Name + ". RegistrationId is : " + student.Name;
                    ViewBag.RegId = student.Name + ". RegistrationId is : " + student.Name;
                    TempData["Success"] = "Registration Successfull";
                    return RedirectToAction("Create");
                    //return RedirectToAction("Details", new { id = student.StudentId });
                }
                else
                {
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentCode", student.DepartmentId);
                    ViewBag.Message = "Email already Exist.";
                    return View();
                }
                
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentCode", student.DepartmentId);
            return View(student);
        }

        private string GenerateRegistrationId(Student student)
        {
            int id =
                db.Students.Count(s => (s.DepartmentId == student.DepartmentId) && (s.Date.Year == student.Date.Year)) + 1;
            Department department = db.Departments.FirstOrDefault(d => d.DepartmentId == student.DepartmentId);
            string registrationId = department.Code + "-" + student.Date.Year + "-";
            
            return registrationId + id.ToString("000");
        }

    }
}
