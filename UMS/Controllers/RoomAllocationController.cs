﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS.Models;

namespace UMS.Controllers
{
    public class RoomAllocationController : Controller
    {
        ProjectDBContext db = new ProjectDBContext();
        public ActionResult UnallocateClassRooms(string submit)
        {
            if (submit == "Unallocate Rooms")
            {
                var rooms = from p in db.RoomAllocations where p.Status.Equals(true) select p;
                foreach (var roomAllocation in rooms)
                {
                    roomAllocation.Status = false;
                    //db.RoomAllocations.AddOrUpdate(roomAllocation);
                    db.Entry(roomAllocation).State = EntityState.Modified;
                    
                }
                db.SaveChanges();
                ViewBag.message = "Sucessfully Unallocated All Class Rooms";

            }
            return View();
        }
        //public string UnallocateClassRooms()
        //{
        //    var rooms = from p in db.RoomAllocations where p.Status.Equals(true) select p;
        //    foreach (var roomAllocation in rooms)
        //    {
        //        roomAllocation.Status = false;
        //        db.RoomAllocations.AddOrUpdate(roomAllocation);
        //    }
        //    db.SaveChanges();
        //    return "Sucessfully Unallocated All Class Rooms";
        //}
       
        //
        // GET: /RoomAllocation/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId","Code");
            ViewBag.CourseId = new SelectList("", "CourseId","Code");
            ViewBag.RoomId = new SelectList(db.Rooms, "RoomId","Name");
            ViewBag.DayId = new SelectList(db.Days, "DayId","Name");

            return View();
        }

        //
        // POST: /RoomAllocation/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoomAllocation roomAllocation)
        {
            Room room = db.Rooms.Find(roomAllocation.RoomId);
            Course course = db.Courses.Find(roomAllocation.CourseId);
            Day day = db.Days.Find(roomAllocation.DayId);
            roomAllocation.Status = true;

            if (ModelState.IsValid)
            {
                int from, end;
                try
                {
                    from = ConvertTimetoInt(roomAllocation.From);
                }
                catch (Exception x)
                {
                    if (TempData["ErrorMessage"] == null)
                    {
                        TempData["ErrorMessage1"] = x.Message;
                    }
                    TempData["ErrorMessage2"] = TempData["ErrorMessage"];
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                    ViewBag.CourseId = new SelectList(db.Courses.Where(c => c.DepartmentId == course.DepartmentId), "CourseId", "Code", roomAllocation.CourseId);
                    ViewBag.RoomId = new SelectList(db.Rooms, "RoomId", "Name", roomAllocation.RoomId);
                    ViewBag.DayId = new SelectList(db.Days, "DayId", "Name", roomAllocation.DayId);
                    return View(roomAllocation);
                }
                try
                {
                    end = ConvertTimetoInt(roomAllocation.To);
                }
                catch (Exception anException)
                {
                    if (TempData["ErrorMessage3"] == null)
                    {
                        TempData["ErrorMessage2"] = anException.Message;
                    }
                    TempData["ErrorMessage5"] = TempData["ErrorMessage3"];
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                    ViewBag.CourseId = new SelectList(db.Courses.Where(c => c.DepartmentId == course.DepartmentId), "CourseId", "Code", roomAllocation.CourseId);
                    ViewBag.RoomId = new SelectList(db.Rooms, "RoomId", "Name", roomAllocation.RoomId);
                    ViewBag.DayId = new SelectList(db.Days, "DayId", "Name", roomAllocation.DayId);
                    return View(roomAllocation);
                }
                if (end < from)
                {
                    TempData["Error"] = "Class Must Should be Started at 24 hours";
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                    ViewBag.CourseId = new SelectList(db.Courses.Where(c => c.DepartmentId == course.DepartmentId), "CourseId", "Code", roomAllocation.CourseId);
                    ViewBag.RoomId = new SelectList(db.Rooms, "RoomId", "Name", roomAllocation.RoomId);
                    ViewBag.DayId = new SelectList(db.Days, "DayId", "Name", roomAllocation.DayId);
                    return View(roomAllocation);
                }
                if (end == from)
                {
                    TempData["Error"] = "Class Must Should be Started at 24 hours";
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                    ViewBag.CourseId = new SelectList(db.Courses.Where(c => c.DepartmentId == course.DepartmentId), "CourseId", "Code", roomAllocation.CourseId);
                    ViewBag.RoomId = new SelectList(db.Rooms, "RoomId", "Name", roomAllocation.RoomId);
                    ViewBag.DayId = new SelectList(db.Days, "DayId", "Name", roomAllocation.DayId);
                    return View(roomAllocation);
                }
                if ((from < 0) || (end >= (24 * 60)))
                {
                    TempData["Message"] = " 24 hours--format HH:MM";
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                    ViewBag.CourseId = new SelectList(db.Courses.Where(c => c.DepartmentId == course.DepartmentId), "CourseId", "Code", roomAllocation.CourseId);
                    ViewBag.RoomId = new SelectList(db.Rooms, "RoomId", "Name", roomAllocation.RoomId);
                    ViewBag.DayId = new SelectList(db.Days, "DayId", "Name", roomAllocation.DayId);
                    return View(roomAllocation);
                }
                List<RoomAllocation> OverLabs = new List<RoomAllocation>();

                var DayRoomAllocationList =
                    db.RoomAllocations.Include(c => c.Course)
                        .Include(d => d.Day)
                        .Include(r => r.Room)
                        .Where(r => r.RoomId == roomAllocation.RoomId && r.DayId == roomAllocation.DayId)
                        .ToList();
                foreach (var aDayroomAllocation in DayRoomAllocationList)
                {
                    if (aDayroomAllocation.Status.Equals(true))
                    {
                        int DbFrom = ConvertTimetoInt(aDayroomAllocation.From);
                        int DbEnd = ConvertTimetoInt(aDayroomAllocation.To);

                        if (
                                ((DbFrom <= from) && (from < DbEnd))
                                || ((DbFrom < end) && (end <= DbEnd))
                                || ((from <= DbFrom) && (DbEnd <= end))
                            )
                        {
                            OverLabs.Add(aDayroomAllocation);
                        }
                    }
                }
                if (OverLabs.Count == 0)
                {

                    db.RoomAllocations.Add(roomAllocation);
                    db.SaveChanges();
                    TempData["Message"] ="Successfull Allocated!!"+ "Room : " + room.Name + " " + "Allocated "
                    + " for course : " + course.Code + " From " + roomAllocation.From
                    + " to " + roomAllocation.To + " on " + day.Name;
                }
                else
                {
                    string message = "Room : " + room.Name ;
                    foreach (var anOverlappedRoom in OverLabs)
                    {
                        int dbFrom = ConvertTimetoInt(anOverlappedRoom.From);
                        int dbEnd = ConvertTimetoInt(anOverlappedRoom.To);

                        string overlapStart, overlapEnd;

                        if ((dbFrom <= from) && (from < dbEnd))
                            overlapStart = roomAllocation.From;
                        else
                            overlapStart = anOverlappedRoom.From;

                        if ((dbFrom < end) && (end <= dbEnd))
                            overlapEnd = roomAllocation.To;
                        else
                            overlapEnd = anOverlappedRoom.To;
                        message += " Course : " + anOverlappedRoom.Course.Code  + " Overlapped from : ";
                        message += overlapStart + " To " + overlapEnd;
                    }

                    TempData["Overlapping"] = message + " on " + day.Name;

                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code");
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                    ViewBag.CourseId = new SelectList(db.Courses.Where(c => c.DepartmentId == course.DepartmentId), "CourseId", "Code", roomAllocation.CourseId);
                    ViewBag.RoomId = new SelectList(db.Rooms, "RoomId", "Name", roomAllocation.RoomId);
                    ViewBag.DayId = new SelectList(db.Days, "DayId", "Name", roomAllocation.DayId);
                    return View(roomAllocation);
                }
                return RedirectToAction("Create");
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", roomAllocation.DepartmentId);
            ViewBag.CourseId = new SelectList(db.Courses, "CourseId", "Code", roomAllocation.CourseId);
            ViewBag.RoomId = new SelectList(db.Rooms, "RoomId", "Name", roomAllocation.RoomId);
            ViewBag.DayId = new SelectList(db.Days, "DayId", "Name", roomAllocation.DayId);
            return View(roomAllocation);
        }

        private int ConvertTimetoInt(string timeStr)
        {
            try
            {
                if (TimeFormateCheck(timeStr))
                {
                    if (timeStr.Length == 4)
                    {
                        timeStr = "0" + timeStr;
                    }
                    string hour = timeStr[0].ToString() + timeStr[1];
                    string minute = timeStr[3].ToString() + timeStr[4];

                    if (Convert.ToInt32(minute) > 59 || Convert.ToInt32(minute) < 0)
                    {
                        TempData["ErrorMessage3"] = "Minites Must Be Equal Or Less Then 59";
                        throw new Exception();
                    }

                    int time = (Convert.ToInt32(hour) * 60);
                    time += Convert.ToInt32(minute);
                    return time;
                }
                else
                {
                    throw new Exception("24 hours--format HH:MM");
                }
            }

            catch (FormatException aFormatException)
            {
                throw new FormatException(
                    "24 hours--format HH:MM", aFormatException);
            }

            catch (IndexOutOfRangeException aRangException)
            {
                throw new IndexOutOfRangeException(
                    "24 hours--format HH:MM", aRangException);
            }

            catch (Exception anException)
            {
                throw new Exception("24 hours--format HH:MM", anException);
            }
        }

        private bool TimeFormateCheck(string timeStr)
        {
            char[] list = timeStr.ToCharArray();
            foreach (var aChar in list)
            {
                if (aChar == ':')
                {
                    return true;
                }
            }
            return false;
        }

        public ActionResult RoomAllocationView()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code");
            List<Course> CourseList = db.Courses.ToList();

            foreach (var aCourse in CourseList)
            {
                aCourse.RoomAllocationList
                    = db.RoomAllocations.Include(a => a.Room).Include(a => a.Day)
                    .Where(a => a.CourseId == aCourse.CourseId && a.Status.Equals(true)).ToList();
            }

            return View(CourseList);
        }

        public ActionResult LoadCourse(int? departmentId)
        {
            var courseList = db.Courses.Where(u => u.DepartmentId == departmentId).ToList();
            ViewBag.CourseId = new SelectList(courseList, "CourseId", "Name");
            return PartialView("~/Views/shared/_FilteredCourse.cshtml");

        }
        public PartialViewResult AllocatedRoomLoad(int? departmentId)
        {
            List<Course> courseList = null;
            if (departmentId != null)
            {
                courseList = db.Courses.Where(c => c.DepartmentId == departmentId).ToList();
            }

            foreach (var aCourse in courseList)
            {
                aCourse.RoomAllocationList
                    = db.RoomAllocations.Include(a => a.Room).Include(a => a.Day)
                    .Where(a => a.CourseId == aCourse.CourseId && a.Status.Equals(true)).ToList();
            }
            if (courseList.Count == 0)
            {
                ViewBag.NoCourse = "Department Empty";
            }
            return PartialView("~/Views/shared/_RoomAllocationView.cshtml", courseList);
        }
    }
}
