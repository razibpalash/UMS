﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS.Models;

namespace UMS.Controllers
{
    public class DepartmentController : Controller
    {
        ProjectDBContext db = new ProjectDBContext();
        //
        // GET: /Department/
        public ActionResult Index()
        {
            List<Department> departments = db.Departments.OrderBy(x => x.Name).ToList();
            return View(departments);
        }

        //
        // GET: /Department/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Department/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Department department)
        {
            if (ModelState.IsValid)
            {
                if (!department.IsExistCode(department.Code)) 
                {
                    if (!department.IsExistName(department.Name))
                    {
                        db.Entry(department).State = EntityState.Added;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.NameMessage = "Department Name Already Exist.";
                        return View();
                    }
                }
                else
                {
                    ViewBag.CodeMessage = "Department Code already Exist.";
                    return View(); 
                }
            }
            else
            {
                return View(department);
            }
        }
    }
}
