﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMS.Models;

namespace UMS.Controllers
{
    public class CourseController : Controller
    {
        ProjectDBContext db = new ProjectDBContext();
        //
        // GET: /Course/
        public ActionResult Index()
        {
            var courses = db.Courses.Include(c => c.Department).Include(c => c.Semester);
            return View(courses.ToList());
        }
        
        //
        // GET: /Course/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code");
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "Name");
            return View();
        }

        //
        // POST: /Course/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseId,Code,Name,Credit,Description,DepartmentId,SemesterId")] Course course)
        {
            course.Status = true;
            var list = from p in db.Semesters where p.SemesterId == course.SemesterId select p;
            foreach (var item in list)
            {
                course.SemName = item.Name;
            }
            if (ModelState.IsValid)
            {
                if (!course.IsExistCode(course.Code)) 
                {
                    if (!course.IsExistName(course.Name))
                    {
                        db.Courses.Add(course);
                        db.SaveChanges();
                        TempData["Success"] = "Added Successfully!!";
                        return RedirectToAction("Create");
                    }
                    else
                    {
                        ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                        ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "Name", course.SemesterId);
                        ViewBag.NameMessage = "Course Name Already Exist";
                        return View();
                    }
                }
                else
                {
                    ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
                    ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "Name", course.SemesterId);
                    ViewBag.Message = "Course code already Exist.";
                    return View(); 
                }
                
            }

            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Code", course.DepartmentId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "SemesterId", "Name", course.SemesterId);
            return View(course);
        }
    }
}
