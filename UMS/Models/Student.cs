﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace UMS.Models
{
    public class Student
    {
        public int StudentId { get; set; }

        [DisplayName("Name"), Required]
        public string Name { get; set; }
        [Required, EmailAddress(ErrorMessage = "Invalid email Address")]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required, DisplayName("Contact No")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?[+]([8]{2})([0]{1})([1]{1})([1-9]{1})([0-9]{8})$", ErrorMessage = "Invalid Phone number. Try this format (+8801XXXXXXXXX)")]
        public string ContactNo { get; set; }

        [Required]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required, DisplayName("Address"), DataType(DataType.MultilineText)]
        public string Address { get; set; }
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }
        public virtual string RegistrationId { get; set; }
        public bool IsExist(string email)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["UMSBDConncetionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string query = "SELECT Email FROM Students WHERE Email ='" + email + "';";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                return true;
            }
            return false;
        }

    }
}