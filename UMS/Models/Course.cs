﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace UMS.Models
{
    public class Course
    {
        public int CourseId { get; set; }

        [Required(ErrorMessage = "Please Enter the Code")]
        [Display(Name = "Code")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Course Code length must be 5 to 20 character loang.")]
        public string Code { get; set; }
        [Required(ErrorMessage = "Please Enter the Name")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Credit")]
        [Range(0.5, 5.0, ErrorMessage = "Credit range must be 0.5 to 5.0.")]
        public double Credit { get; set; }
        [Required, DisplayName("Description"), DataType(DataType.MultilineText)]
        public string Description { get; set; }
        
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public int SemesterId { get; set; }
        public virtual Semester Semester { get; set; }
        public List<RoomAllocation> RoomAllocationList { get; set; }
        public string AssignTo { get; set; }
        public virtual Semester CourseSemister { get; set; }
        public virtual ICollection<Teacher> CourseTeacher { get; set; }
        public bool Status { get; set; }
        public string SemName { get; set; }

        public bool IsExistCode(string code)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["UMSBDConncetionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string query = "SELECT * FROM Courses WHERE Code ='" + code + "';";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                return true;
            }
            return false;
        }

        public bool IsExistName(string name)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["UMSBDConncetionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string query = "SELECT * FROM Courses WHERE Name ='" + name + "';";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                return true;
            }
            return false;
        }

    }
}