﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace UMS.Models
{
    public class Teacher
    {
        public int TeacherId { get; set; }

        [Required(ErrorMessage = "Please Enter The Name.")]
        public string Name { get; set; }
        [Required, DisplayName("Address"), DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your Contact No"), DisplayName("Contact No")]
        [RegularExpression(@"^\(?[+]([8]{2})([0]{1})([1]{1})([1-9]{1})([0-9]{8})$",
           ErrorMessage = "Invalid Phone number. Try this format (+8801XXXXXXXXX)")]
        public string ContactNo { get; set; }
        [DisplayName("Designation")]
        public int DesignationId { get; set; }
        public virtual Designation Designation { get; set; }
        [DisplayName("Department")]
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        [Required, DisplayName("Cradit To Be Taken")]
        [Range(5, 20, ErrorMessage = "Invalid Cradit")]
        public double CreditTaken { get; set; }
        public virtual ICollection<Course> TeachersCourses { get; set; }

        public bool IsExist(string email)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["UMSBDConncetionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string query = "SELECT Email FROM Teachers WHERE Email ='" + email + "';";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                return true;
            }
            return false;
        }

    }
}