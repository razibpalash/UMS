﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace UMS.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        [Required(ErrorMessage = "Please Enter the Code")]
        [Display(Name = "Code")]
        [StringLength(7, MinimumLength = 2, ErrorMessage = "Code must be 2 to 7 character length")]
        public string Code { get; set; }
        [Required(ErrorMessage = "Please Enter the Name")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public virtual ICollection<Course> Courses { get; set; } 

        public bool IsExistCode(string code)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["UMSBDConncetionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string query = "SELECT Code FROM Departments WHERE Code ='" + code + "';";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows)
            {
                reader.Read();
                return true;
            }
            return false;
        }

        public bool IsExistName(string name)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["UMSBDConncetionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string query = "SELECT Name FROM Departments WHERE Name ='" + name + "';";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                return true;
            }
            return false;
        }
    }
}