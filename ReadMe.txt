 University Management System(Team Project)

Used HTML, CSS, and JavaScript in client side and Microsoft SQL Server as database and ASP.NET MVC 5 as server side language. Can be used to assign courses to teachers and students and allocate classrooms to courses. Can be used to maintain and generate a pdf format result sheet of a student.